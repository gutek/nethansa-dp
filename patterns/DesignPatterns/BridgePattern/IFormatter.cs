namespace BridgePattern
{
    // other abstraction
    public interface IFormatter
    {
        string Format(string key, string value);
    }
}