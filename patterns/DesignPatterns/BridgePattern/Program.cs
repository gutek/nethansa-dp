using System;
using System.Collections.Generic;

namespace BridgePattern
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Manuscript> documents = new List<Manuscript>();
            var formatter = new BackwardsFormatter();

            var faq = new Faq(formatter);
            faq.Title = "The Bridge Pattern FAQ";
            faq.Questions.Add("What is it?", "A design pattern");
            faq.Questions.Add("When do we use it?", "When you need to separate two abstractions from implementation.");
            documents.Add(faq);

            var book = new Book(formatter)
            {
                Title = "Pan Tadeusz",
                Author = "Adam Micksiewicz",
                Text = "Litwo oczyzno moja..."
            };
            documents.Add(book);

            var paper = new TermPaper(formatter)
            {
                Class = "Ojczyzna w lekturach",
                Student = "Janek0001",
                Text = "Nie moge tego wytrzymac, mam dosc...",
                References = "Pan Tadeusz"
            };
            documents.Add(paper);

            foreach (var doc in documents)
            {
                doc.Print();
            }

            Console.ReadKey();
        }
    }
}
