using System;

namespace ChainOfResponsibilityPattern
{
    public class Dad : ChainOfCommand
    {
        public override void ProcessRequest(Purchase purchase)
        {
            if (purchase.Amount < 25000)
            {
                Console.WriteLine("{0} approved request {1}", GetType().Name, purchase.Purpose);
            }
            else
            {
                _nextInLine?.ProcessRequest(purchase);
            }
        }
    }
}