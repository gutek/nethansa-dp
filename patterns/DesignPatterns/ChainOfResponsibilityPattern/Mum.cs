using System;

namespace ChainOfResponsibilityPattern
{
    public class Mum : ChainOfCommand
    {
        public override void ProcessRequest(Purchase purchase)
        {
            if (purchase.Amount < 100000)
            {
                Console.WriteLine("{0} approved request {1}", GetType().Name, purchase.Purpose);
            }
            else
            {
                Console.WriteLine("Request# {0} requires FLOWERS, LOTS OF FLOWERS!! and SPA!", purchase.Purpose);
            }
        }
    }
}