namespace ChainOfResponsibilityPattern
{
    public abstract class ChainOfCommand
    {
        protected ChainOfCommand _nextInLine;

        public void WoToAsk(ChainOfCommand successor)
        {
            _nextInLine = successor;
        }

        public abstract void ProcessRequest(Purchase purchase);
    }
}