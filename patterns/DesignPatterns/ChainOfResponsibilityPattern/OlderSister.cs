using System;

namespace ChainOfResponsibilityPattern
{
    public class OlderSister : ChainOfCommand
    {
        public override void ProcessRequest(Purchase purchase)
        {
            if (purchase.Amount < 1000)
            {
                Console.WriteLine("{0} approved request {1}", GetType().Name, purchase.Purpose);
            }
            else
            {
                _nextInLine?.ProcessRequest(purchase);
            }
        }
    }
}