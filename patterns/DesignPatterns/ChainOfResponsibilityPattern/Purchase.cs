namespace ChainOfResponsibilityPattern
{
    public class Purchase
    {
        public Purchase(double amount, string purpose)
        {
            Amount = amount;
            Purpose = purpose;
        }

        public double Amount { get; }
        public string Purpose { get; }
    }
}