using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChainOfResponsibilityPattern
{
    class Program
    {
        static void Main(string[] args)
        {
            ChainOfCommand mummy = new Mum();
            ChainOfCommand daddy = new Dad();
            ChainOfCommand sister = new OlderSister();

            sister.WoToAsk(daddy);
            daddy.WoToAsk(mummy);

            Purchase p = new Purchase(10, "Blue Track");
            sister.ProcessRequest(p);

            p = new Purchase(32590.10, "Tractor!");
            sister.ProcessRequest(p);

            p = new Purchase(122100.00, "Bentley");
            sister.ProcessRequest(p);

            Console.ReadKey();
        }
    }
}
