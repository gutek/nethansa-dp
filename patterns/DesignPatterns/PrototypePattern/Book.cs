using System;

namespace PrototypePattern
{
    public interface IBook
    {
        string Author { get; }
        string Title { get; }
        int PageCount { get; }
    }

    public class Book : IBook, ICloneable
    {
        public string Title { get; }
        public string Author { get; }
        public int PageCount { get; }

        public Book(string title, string author, int pageCount)
        {
            Title = title;
            Author = author;
            PageCount = pageCount;
        }

        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }
}