using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace PrototypePattern
{
    class Program
    {
        static void Main(string[] args)
        {
            var book = new Book("Pan Tadeusz", "Adam Mickiewicz", 296);

            var clone = book.Clone();
            Console.WriteLine("---- Base Book ----");
            Console.WriteLine(JsonConvert.SerializeObject(book));
            Console.WriteLine("---- Shallow Clone ----");
            Console.WriteLine(JsonConvert.SerializeObject(clone));
            Console.ReadKey();
        }
    }
}
