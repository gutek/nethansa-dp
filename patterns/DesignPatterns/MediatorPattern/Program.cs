using System;

namespace MediatorPattern
{
    class Program
    {
        static void Main(string[] args)
        {
            SnackKiosksMediator mediator = new SnackKiosksMediator();

            NorthSnackKiosk leftKitchen = new NorthSnackKiosk(mediator);
            SouthSnackKiosk rightKitchen = new SouthSnackKiosk(mediator);

            mediator.NorthSnackKiosks = leftKitchen;
            mediator.SouthSnackKiosks = rightKitchen;

            leftKitchen.Send("Potrzebuje popcornu, przyniesiecie??");
            rightKitchen.Send("Jasne, już wysyłam Janka!");

            rightKitchen.Send("Macie hotdogi? Ludzie chca wydac wszystkie pieniadze na nie tutaj, wiec sa nam potrzebne!");
            leftKitchen.Send("Mało :( Wyślemy znów Janka do was z nimi");

            Console.ReadKey();
        }
    }
}
