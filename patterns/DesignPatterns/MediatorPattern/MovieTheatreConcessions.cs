﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MediatorPattern
{
    /// <summary>
    /// The Mediator interface, which defines a send message method which the concrete mediators must implement.
    /// </summary>
    interface Mediator
    {
        void SendMessage(string message, SnackKiosk snackKiosk);
    }

    /// <summary>
    /// The Concrete Mediator class, which implement the send message method and keep track of all participants in the conversation.
    /// </summary>
    class SnackKiosksMediator : Mediator
    {
        private NorthSnackKiosk _northSnackKiosks;
        private SouthSnackKiosk _southSnackKiosks;

        public NorthSnackKiosk NorthSnackKiosks
        {
            set => _northSnackKiosks = value;
        }

        public SouthSnackKiosk SouthSnackKiosks
        {
            set => _southSnackKiosks = value;
        }

        public void SendMessage(string message, SnackKiosk colleague)
        {
            if (colleague == _northSnackKiosks)
            {
                _southSnackKiosks.Notify(message);
            }
            else
            {
                _northSnackKiosks.Notify(message);
            }
        }
    }

    /// <summary>
    /// The Colleague abstract class, representing an entity involved in the conversation which should receive messages.
    /// </summary>
    abstract class SnackKiosk
    {
        protected Mediator _mediator;

        protected SnackKiosk(Mediator mediator)
        {
            _mediator = mediator;
        }
    }

    /// <summary>
    /// A Concrete Colleague class
    /// </summary>
    class NorthSnackKiosk : SnackKiosk
    {
        // Constructor
        public NorthSnackKiosk(Mediator mediator) : base(mediator)
        {
        }

        public void Send(string message)
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("North Snack Kiosk sends message: " + message);
            Console.ResetColor();
            _mediator.SendMessage(message, this);
        }

        public void Notify(string message)
        {
            Console.BackgroundColor = ConsoleColor.Yellow;
            Console.ForegroundColor = ConsoleColor.Black;
            Console.WriteLine("North Snack Kiosk gets message: " + message);
            Console.ResetColor();
        }
    }

    /// <summary>
    /// A Concrete Colleague class
    /// </summary>
    class SouthSnackKiosk : SnackKiosk
    {
        public SouthSnackKiosk(Mediator mediator) : base(mediator)
        {
        }

        public void Send(string message)
        {
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.WriteLine("South Snack Kiosk sends message: " + message);
            Console.ResetColor();
            _mediator.SendMessage(message, this);
        }

        public void Notify(string message)
        {
            Console.BackgroundColor = ConsoleColor.Gray;
            Console.ForegroundColor = ConsoleColor.Black;
            Console.WriteLine("South Snack Kiosk gets message: " + message);
            Console.ResetColor();
        }
    }
}
