using System;
using System.Collections.Generic;

namespace IteratorPattern
{
    public static class ClassPropExtensions
    {
        public static IList<Tuple<string, object>> GetProps(this object @this)
        {
            var props = @this.GetType().GetProperties();
            var list = new List<Tuple<string, object>>();
            foreach (var prop in props)
            {
                var val = prop.GetValue(@this, null);
                var t = new Tuple<string, object>(prop.Name, val);
                list.Add(t);
            }

            return list;
        }
    }
}