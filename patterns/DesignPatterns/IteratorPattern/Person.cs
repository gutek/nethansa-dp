using System;
using System.Collections;
using System.Collections.Generic;

namespace IteratorPattern
{
    public class Person : IEnumerable<Tuple<string, object>>
    {
        public string Name { get; set; }
        public int Age { get; set; }

        public Person(string name, int age)
        {
            Name = name;
            Age = age;
        }

        public IEnumerator<Tuple<string, object>> GetEnumerator() => new ClassEnumerator(this);

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}