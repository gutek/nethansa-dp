using System;

namespace IteratorPattern
{
    class Program
    {
        static void Main(string[] args)
        {
            var person = new Person("Tomek", 10);

            Console.WriteLine("Tomek details:");
            foreach (var p in person)
            {
                Console.WriteLine($"\t{p.Item1}:{p.Item2}");
            }

            Console.ReadKey();
        }
    }
}
