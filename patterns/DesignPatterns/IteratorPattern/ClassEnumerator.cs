using System;
using System.Collections;
using System.Collections.Generic;

namespace IteratorPattern
{
    public class ClassEnumerator : IEnumerator<Tuple<string, object>>
    {
        private readonly IList<Tuple<string, object>> _props;
        private readonly int _length;

        private int _index = -1;

        public ClassEnumerator(object myClass)
        {
            _props = myClass.GetProps();
            _length = _props.Count;
        }

        public bool MoveNext()
        {
            _index++;

            return _index < _length;
        }

        public void Reset() => _index = -1;
        public Tuple<string, object> Current => _props[_index];
        object IEnumerator.Current => Current;
        public void Dispose() { }
    }
}