using System;

namespace ProxyPattern
{
    class Program
    {
        static void Main(string[] args)
        {
            Client client = new Client();

            Console.WriteLine("Client: Executing the client code with a real service:");
            var service = new RealService();
            client.ClientCode(service);

            Console.WriteLine();

            Console.WriteLine("Client: Executing the same client code with a proxy:");
            Proxy proxy = new Proxy(service);
            client.ClientCode(proxy);

            Console.ReadKey();
        }
    }
}
