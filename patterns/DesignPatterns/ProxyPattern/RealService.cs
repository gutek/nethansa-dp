using System;

namespace ProxyPattern
{
    // The RealService contains some core business logic. Usually, RealSubjects
    // are capable of doing some useful work which may also be very slow or
    // sensitive - e.g. correcting input data. A Proxy can solve these issues
    // without any changes to the RealService's code.
    public class RealService : IService
    {
        public void Request()
        {
            Console.WriteLine("RealService: Handling Request.");
        }
    }
}