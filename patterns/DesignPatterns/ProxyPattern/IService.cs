namespace ProxyPattern
{
    public interface IService
    {
        void Request();
    }
}