namespace ProxyPattern
{
    public class Client
    {
        public void ClientCode(IService service)
        {
            service.Request();
        }
    }
}