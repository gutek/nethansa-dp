using System;

namespace ProxyPattern
{
    public class Proxy : IService
    {
        private readonly RealService _realService;

        public Proxy(RealService realService)
        {
            _realService = realService;
        }

        public void Request()
        {
            if (!CheckAccess()) return;

            _realService.Request();

            LogAccess();
        }

        public bool CheckAccess()
        {
            Console.WriteLine("Proxy: Checking access prior to firing a real request.");

            return true;
        }

        public void LogAccess()
        {
            Console.WriteLine("Proxy: Logging the time of request.");
        }
    }
}