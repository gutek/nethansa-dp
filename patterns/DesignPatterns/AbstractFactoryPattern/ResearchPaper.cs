using System;

namespace AbstractFactoryPattern
{
    public class ResearchPaper : IBook
    {
        public string Author { get; set; }
        public string Title { get; set; }

        public ResearchPaper(string author, string title)
        {
            Author = author;
            Title = title;
            Console.WriteLine($"Made an IBook of type: {this}.");
        }
    }
}