namespace AbstractFactoryPattern
{
    public interface IBook
    {
        string Author { get; set; }
        string Title { get; set; }
    }
}