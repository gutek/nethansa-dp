using System;

namespace AbstractFactoryPattern
{
    public class PoemFactory : IBookFactory
    {
        public PoemFactory()
        {
        }

        public PoemFactory(string author, string title, string publisher)
        {
            MakeBook(author: author, title: title);
            MakePublisher(name: publisher);
            Console.WriteLine($"Made an IBookFactory of type: {this}.");
        }

        public IBook MakeBook(string author, string title)
        {
            return new Poem(author: author, title: title);
        }

        public IPublisher MakePublisher(string name)
        {
            return new Blog(name: name);
        }
    }
}