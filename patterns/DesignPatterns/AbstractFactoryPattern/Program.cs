using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbstractFactoryPattern
{
    class Program
    {
        static void Main(string[] args)
        {
            var poemFactory = new PoemFactory(author: "Adam Mickiewicz",
                title: "Dziady",
                publisher: "Adam Mickiewicz Pub");

            var researchFactory= new ResearchPaperFactory(author: "Gang of Four",
                title: "Design Patterns: Elements of Reusable Object-Oriented Software",
                publisher: "Addison-Wesley");

            Console.ReadKey();
        }
    }
}
