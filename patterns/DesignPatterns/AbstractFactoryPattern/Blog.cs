using System;

namespace AbstractFactoryPattern
{
    public class Blog : IPublisher
    {
        public string Name { get; set; }

        public Blog(string name)
        {
            Name = name;
            Console.WriteLine($"Made an IPublisher of type: {this}.");
        }
    }
}