using System;

namespace AbstractFactoryPattern
{
    public class ResearchPaperFactory : IBookFactory
    {
        public ResearchPaperFactory()
        {
        }

        public ResearchPaperFactory(string author, string title, string publisher)
        {
            MakeBook(author: author, title: title);
            MakePublisher(name: publisher);
            Console.WriteLine($"Made an IBookFactory of type: {this}.");
        }

        public IBook MakeBook(string author, string title)
        {
            return new ResearchPaper(author: author, title: title);
        }

        public IPublisher MakePublisher(string name)
        {
            return new ScientificJournal(name: name);

        }
    }
}