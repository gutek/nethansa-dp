namespace AbstractFactoryPattern
{
    public interface IPublisher
    {
        string Name { get; set; }
    }
}