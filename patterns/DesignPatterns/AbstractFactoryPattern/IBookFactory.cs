namespace AbstractFactoryPattern
{
    public interface IBookFactory
    {
        IBook MakeBook(string author, string title);
        IPublisher MakePublisher(string name);
    }
}