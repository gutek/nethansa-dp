using System;

namespace StatePattern
{
    // Concreate state, that implements its own behaviour
    public class DeadState : State
    {
        public override void Die()
        {
            Console.Write("DeadState handles die - how many time's can I die?.");
        }

        public override void Live()
        {
            Console.WriteLine("DeadState handles Resurrection.");
            Console.WriteLine("DeadState wants to change the state of the Person to Alive again.");
            Person.TransitionTo(new AliveState());
        }
    }
}