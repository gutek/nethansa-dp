using System;

namespace StatePattern
{
    // Concreate state, that implements its own behaviour
    public class AliveState : State
    {
        public override void Die()
        {
            Console.WriteLine("AliveState handles die.");
            Console.WriteLine("AliveState wants to change the state of the Person to dead.");
            Person.TransitionTo(new DeadState());
        }

        public override void Live()
        {
            Console.WriteLine("AliveState handles live.");
        }
    }
}