namespace StatePattern
{
    // Base state with reference to person - owner, class
    // so we can switch states from states
    public abstract class State
    {
        protected Person Person;

        public void SetContext(Person person)
        {
            this.Person = person;
        }

        public abstract void Die();

        public abstract void Live();
    }
}