using System;

namespace StatePattern
{
    public class Person
    {
        // A reference to the current state of the Person.
        private State _state = null;

        public Person(State state)
        {
            this.TransitionTo(state);
        }

        // The Person allows changing the State object at runtime.
        public void TransitionTo(State state)
        {
            Console.WriteLine($"Person: Transition to {state.GetType().Name}.");
            this._state = state;
            this._state.SetContext(this);
        }

        // The Person delegates part of its behavior to the current State
        // object.
        public void Die()
        {
            this._state.Die();
        }

        public void Live()
        {
            this._state.Live();
        }
    }
}