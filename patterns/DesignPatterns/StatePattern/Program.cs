using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StatePattern
{
    class Program
    {
        static void Main(string[] args)
        {

            // The client code.
            var person = new Person(new AliveState());
            person.Die();
            person.Live();
            person.Live();

            Console.ReadKey();
        }
    }

    
}
