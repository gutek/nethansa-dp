using System;

namespace TemplateMethodPattern
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Sending order UPS: ");
            var upsOrder = new UpsOrderShipment();
            upsOrder.ShippingAddress = "Pilsudskiego, 02-798 Warszawa";
            upsOrder.Ship();
            Console.WriteLine("----------------------------------------");

            Console.WriteLine("Sending order Inpost: ");
            var inpostOrder = new InpostOrderShipment();
            inpostOrder.ShippingAddress = "Mackowicka, 02-798 Warszawa";
            inpostOrder.Ship();
            Console.WriteLine("----------------------------------------");

            Console.ReadKey();
        }
    }
}
