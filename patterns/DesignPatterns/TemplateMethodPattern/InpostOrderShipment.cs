namespace TemplateMethodPattern
{
    public class InpostOrderShipment : OrderShipment
    {
        public override void GetShippingLabelFromCarrier()
        {
            Label = $"Inpost:[{ShippingAddress}]";
        }
    }
}