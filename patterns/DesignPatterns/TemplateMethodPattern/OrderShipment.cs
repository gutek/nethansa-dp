using System;

namespace TemplateMethodPattern
{
    public abstract class OrderShipment
    {
        public string ShippingAddress { get; set; }
        public string Label { get; set; }
        public void Ship()
        {
            VerifyShippingData();
            GetShippingLabelFromCarrier();
            PrintLabel();
        }

        public virtual void VerifyShippingData()
        {
            if (string.IsNullOrEmpty(ShippingAddress))
            {
                throw new ApplicationException("Invalid address.");
            }
        }
        public abstract void GetShippingLabelFromCarrier();
        public virtual void PrintLabel()
        {
            Console.WriteLine(Label);
        }
    }
}