﻿using System;

namespace ObeserverPattern
{
    class Program
    {
        static void Main(string[] args)
        {
            // Create price watch for Carrots and attach restaurants that buy carrots from suppliers.
            Carrots carrots = new Carrots(0.82);
            carrots.Attach(new Restaurant("Res1", 0.77));
            carrots.Attach(new Restaurant("Res2", 0.74));
            carrots.Attach(new Restaurant("Res3 ", 0.75));

            // Fluctuating carrot prices will notify subscribing restaurants.
            carrots.PricePerPound = 0.79;
            carrots.PricePerPound = 0.76;
            carrots.PricePerPound = 0.74;
            carrots.PricePerPound = 0.81;

            BaggageHandler provider = new BaggageHandler();
            ArrivalsMonitor observer1 = new ArrivalsMonitor("BaggageClaimMonitor");
            ArrivalsMonitor observer2 = new ArrivalsMonitor("SecurityExit");

            provider.BaggageStatus(712, "Warszawa", 3);
            observer1.Subscribe(provider);

            provider.BaggageStatus(712, "Londyn", 3);
            provider.BaggageStatus(400, "Krakow", 1);
            provider.BaggageStatus(712, "Gdansk", 3);
            observer2.Subscribe(provider);

            provider.BaggageStatus(511, "Radom", 2);
            provider.BaggageStatus(712);

            observer2.Unsubscribe();

            provider.BaggageStatus(400);
            provider.LastBaggageClaimed();

            Console.ReadKey();
        }
    }
}
