﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ObeserverPattern
{
    public class ArrivalsMonitor : IObserver<BaggageInfo>
    {
        private readonly string _name;
        private readonly List<string> _flightInfos = new List<string>();
        private IDisposable _cancellation;
        private string fmt = "{0,-20} {1,5}  {2, 3}";

        public ArrivalsMonitor(string name)
        {
            if (string.IsNullOrEmpty(name)) throw new ArgumentNullException("The observer must be assigned a name.");

            _name = name;
        }

        public virtual void Subscribe(BaggageHandler provider)
        {
            _cancellation = provider.Subscribe(this);
        }

        public virtual void Unsubscribe()
        {
            _cancellation.Dispose();
            _flightInfos.Clear();
        }

        public virtual void OnCompleted()
        {
            _flightInfos.Clear();
        }

        // No implementation needed: Method is not called by the BaggageHandler class.
        public virtual void OnError(Exception e)
        {
            // No implementation.
        }

        // Update information.
        public virtual void OnNext(BaggageInfo info)
        {
            bool updated = false;

            // Flight has unloaded its baggage; remove from the monitor.
            if (info.Carousel == 0)
            {
                var flightsToRemove = new List<string>();
                string flightNo = $"{info.FlightNumber,5}";

                foreach (var flightInfo in _flightInfos)
                {
                    if (flightInfo.Substring(21, 5).Equals(flightNo))
                    {
                        flightsToRemove.Add(flightInfo);
                        updated = true;
                    }
                }
                foreach (var flightToRemove in flightsToRemove)
                    _flightInfos.Remove(flightToRemove);

                flightsToRemove.Clear();
            }
            else
            {
                // Add flight if it does not exist in the collection.
                string flightInfo = string.Format(fmt, info.From, info.FlightNumber, info.Carousel);
                if (!_flightInfos.Contains(flightInfo))
                {
                    _flightInfos.Add(flightInfo);
                    updated = true;
                }
            }
            if (updated)
            {
                _flightInfos.Sort();
                Console.WriteLine("Arrivals information from {0}", _name);
                foreach (var flightInfo in _flightInfos)
                    Console.WriteLine(flightInfo);

                Console.WriteLine();
            }
        }
    }

    internal class Unsubscriber<BaggageInfo> : IDisposable
    {
        private readonly List<IObserver<BaggageInfo>> _observers;
        private readonly IObserver<BaggageInfo> _observer;

        internal Unsubscriber(List<IObserver<BaggageInfo>> observers, IObserver<BaggageInfo> observer)
        {
            this._observers = observers;
            this._observer = observer;
        }

        public void Dispose()
        {
            if (_observers.Contains(_observer))
                _observers.Remove(_observer);
        }
    }

    public class BaggageHandler : IObservable<BaggageInfo>
    {
        private readonly List<IObserver<BaggageInfo>> _observers;
        private readonly List<BaggageInfo> _flights;

        public BaggageHandler()
        {
            _observers = new List<IObserver<BaggageInfo>>();
            _flights = new List<BaggageInfo>();
        }

        public IDisposable Subscribe(IObserver<BaggageInfo> observer)
        {
            // Check whether observer is already registered. If not, add it
            if (!_observers.Contains(observer))
            {
                _observers.Add(observer);
                // Provide observer with existing data.
                foreach (var item in _flights)
                    observer.OnNext(item);
            }
            return new Unsubscriber<BaggageInfo>(_observers, observer);
        }

        // Called to indicate all baggage is now unloaded.
        public void BaggageStatus(int flightNo)
        {
            BaggageStatus(flightNo, String.Empty, 0);
        }

        public void BaggageStatus(int flightNo, string from, int carousel)
        {
            var info = new BaggageInfo(flightNo, from, carousel);

            // Carousel is assigned, so add new info object to list.
            if (carousel > 0 && !_flights.Contains(info))
            {
                _flights.Add(info);
                foreach (var observer in _observers)
                    observer.OnNext(info);
            }
            else if (carousel == 0)
            {
                // Baggage claim for flight is done
                var flightsToRemove = new List<BaggageInfo>();
                foreach (var flight in _flights)
                {
                    if (info.FlightNumber == flight.FlightNumber)
                    {
                        flightsToRemove.Add(flight);
                        foreach (var observer in _observers)
                            observer.OnNext(info);
                    }
                }
                foreach (var flightToRemove in flightsToRemove)
                    _flights.Remove(flightToRemove);

                flightsToRemove.Clear();
            }
        }

        public void LastBaggageClaimed()
        {
            foreach (var observer in _observers)
                observer.OnCompleted();

            _observers.Clear();
        }
    }

    public class BaggageInfo
    {
        internal BaggageInfo(int flight, string from, int carousel)
        {
            FlightNumber = flight;
            From = from;
            Carousel = carousel;
        }

        public int FlightNumber { get; }

        public string From { get; }

        public int Carousel { get; }
    }
}
