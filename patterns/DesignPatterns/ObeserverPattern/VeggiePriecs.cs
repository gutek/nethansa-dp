﻿using System;
using System.Collections.Generic;

namespace ObeserverPattern
{
    /// <summary>
    /// The Subject abstract class
    /// </summary>
    abstract class Veggies
    {
        private double _pricePerKg;
        private readonly List<IRestaurant> _restaurants = new List<IRestaurant>();

        protected Veggies(double pricePerKg)
        {
            _pricePerKg = pricePerKg;
        }

        public void Attach(IRestaurant restaurant)
        {
            _restaurants.Add(restaurant);
        }

        public void Detach(IRestaurant restaurant)
        {
            _restaurants.Remove(restaurant);
        }

        public void Notify()
        {
            foreach (IRestaurant restaurant in _restaurants)
            {
                restaurant.Update(this);
            }

            Console.WriteLine("");
        }

        public double PricePerPound
        {
            get => _pricePerKg;
            set
            {
                if (_pricePerKg == value) return;
                _pricePerKg = value;
                Notify();
            }
        }
    }

    /// <summary>
    /// The ConcreteSubject class
    /// </summary>
    class Carrots : Veggies
    {
        public Carrots(double price) : base(price)
        {
        }
    }

    /// <summary>
    /// The Observer interface
    /// </summary>
    interface IRestaurant
    {
        void Update(Veggies veggies);
    }

    /// <summary>
    /// The ConcreteObserver class
    /// </summary>
    class Restaurant : IRestaurant
    {
        private readonly string _name;
        private readonly double _purchaseThreshold;

        private Veggies _veggie;

        public Restaurant(string name, double purchaseThreshold)
        {
            _name = name;
            _purchaseThreshold = purchaseThreshold;
        }

        public void Update(Veggies veggie)
        {
            Console.WriteLine("Notified {0} of {1}'s " + " price change to {2:C} per kg.", _name, veggie.GetType().Name, veggie.PricePerPound);
            if (veggie.PricePerPound < _purchaseThreshold)
            {
                Console.WriteLine(_name + " wants to buy some " + veggie.GetType().Name + "!");
            }
        }
    }
}
