using System;

namespace MementoPattern
{
    public interface IMemento
    {
        string Name { get; }
        string State { get; }
        DateTime Date { get; }
    }
}