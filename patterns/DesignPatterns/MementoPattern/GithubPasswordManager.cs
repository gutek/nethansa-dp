using System;
using System.Collections.Generic;
using System.Linq;

namespace MementoPattern
{
    public class GithubPasswordManager
    {
        private readonly List<IMemento> _mementos = new List<IMemento>();
        private readonly Password _password;

        public GithubPasswordManager(Password password)
        {
            _password = password;
        }

        public void SaveCurrentPassowrd()
        {
            Console.WriteLine();
            Console.WriteLine("GithubPasswordManager: Saving current password...");
            _mementos.Add(this._password.Save());
        }

        public void Undo()
        {
            if (_mementos.Count == 0)
            {
                return;
            }

            var memento = _mementos.Last();
            _mementos.Remove(memento);

            Console.WriteLine("GithubPasswordManager: Restoring password to: " + memento.Name);

            try
            {
                _password.Restore(memento);
            }
            catch (Exception)
            {
                this.Undo();
            }
        }

        public void ShowHistory()
        {
            Console.WriteLine("GithubPasswordManager: Here's the list of passwords used:");

            foreach (var memento in this._mementos)
            {
                Console.WriteLine(memento.Name);
            }
        }
    }
}