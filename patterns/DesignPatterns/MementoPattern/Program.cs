using System;

namespace MementoPattern
{
    class Program
    {
        static void Main(string[] args)
        {
            var password = new Password("My First Amazing Password");
            var manager = new GithubPasswordManager(password);

            manager.SaveCurrentPassowrd();
            password.GenerateNewPassword();

            manager.SaveCurrentPassowrd();
            password.GenerateNewPassword();

            manager.SaveCurrentPassowrd();
            password.GenerateNewPassword();

            Console.WriteLine();
            manager.ShowHistory();

            Console.WriteLine("");
            Console.WriteLine("I want my pre password BACK!");
            Console.WriteLine("");
            manager.Undo();

            Console.WriteLine("");
            Console.WriteLine("");
            Console.WriteLine("Not this GODDAMNED, prev one!");
            Console.WriteLine("");
            manager.Undo();

            Console.ReadKey();
        }
    }
}
