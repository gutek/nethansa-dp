using System;

namespace MementoPattern
{
    public class PasswordHistory : IMemento
    {
        public PasswordHistory(string state)
        {
            State = state;
            Date = DateTime.Now;
        }

        public string State { get; }
        public string Name => $"{Date} / ({State.Substring(0, 9)})...";

        public DateTime Date { get; }
    }
}