using System;
using System.Threading;

namespace MementoPattern
{
    public class Password
    {
        private string _state;

        public Password(string state)
        {
            _state = state;
            Console.WriteLine("Password: My initial password is: " + state);
        }

        // internal change of the state, so it should be save
        public void GenerateNewPassword()
        {
            Console.WriteLine("Password: I'm doing something important.");
            _state = GenerateRandomString(30);
            Console.WriteLine($"Password: and my password has changed to: {_state}");
        }

        public IMemento Save()
        {
            return new PasswordHistory(_state);
        }

        public void Restore(IMemento memento)
        {
            var m = memento as PasswordHistory;
            if (m == null)
            {
                throw new Exception("Unknown memento class " + memento);
            }
            _state = memento.State;
            Console.Write($"Password: My password has changed to: {memento.State}");
        }
        
        private string GenerateRandomString(int length = 10)
        {
            string allowedSymbols = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
            string result = string.Empty;

            while (length > 0)
            {
                result += allowedSymbols[new Random().Next(0, allowedSymbols.Length)];

                Thread.Sleep(1);

                length--;
            }

            return result;
        }
    }
}