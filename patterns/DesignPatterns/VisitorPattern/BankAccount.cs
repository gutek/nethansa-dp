﻿using VisitorPattern.Visitor;

namespace VisitorPattern
{
    public class BankAccount : IAsset
    {
        public int Balance { get; set; }
        public double MonthlyInterest { get; set; }

        public void Accept(IVisitor visitor)
        {
            visitor.Visit(this);
        }
    }
}