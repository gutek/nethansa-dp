using System;
using VisitorPattern.Visitor;

namespace VisitorPattern
{
    class Program
    {
        static void Main(string[] args)
        {
            var person = new Person();
            person.Assets.Add(new BankAccount { Balance = 1000, MonthlyInterest = 0.01 });
            person.Assets.Add(new BankAccount { Balance = 2000, MonthlyInterest = 0.02 });
            person.Assets.Add(new RealEstate { EstimatedValue = 79000, MonthlyRent = 500 });
            person.Assets.Add(new Loan { Owed = 40000, MonthlyPayment = 40 });

            var netWorthVisitor = new NetWorthVisitor();
            var incomeVisitor = new IncomeVisitor();

            person.Accept(netWorthVisitor);
            person.Accept(incomeVisitor);

            Console.WriteLine($"net worth: {netWorthVisitor.Total:C}");
            Console.WriteLine($"income: {incomeVisitor.Amount:C}");
            Console.ReadLine();
        }
    }
}
