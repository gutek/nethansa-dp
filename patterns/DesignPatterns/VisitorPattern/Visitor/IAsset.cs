﻿namespace VisitorPattern.Visitor
{
    public interface IAsset
    {
        void Accept(IVisitor visitor);
    }
}