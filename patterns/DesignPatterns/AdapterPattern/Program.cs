using System;

namespace AdapterPattern
{
    class Program
    {
        static void Main(string[] args)
        {
            // Create new XBox console.
            var xbox = new XBox();
            // Play with default controller.
            xbox.Play();
            // Create new PlayStationController.
            var playStationController = new PlayStationController();
            // Create controller adapter for PlayStation controller.
            var adapter = new ControllerAdapter(playStationController);
            // Play with adapted controller.
            xbox.Play(adapter);

            Console.WriteLine("----------------------------");

            // Create new PlayStation console.
            var playstation = new PlayStation();
            // Play with default controller.
            playstation.Play();
            // Create new XBoxController.
            var xboxController = new XBoxController();
            // Create controller adapter for XBox controller.
            var adapter2 = new ControllerAdapter(xboxController);
            // Play with adapted controller.
            playstation.Play(adapter2);

            Console.ReadKey();
        }
    }

}
