using System;

namespace AdapterPattern
{
    /// <summary>
    /// Used to adapt incompatible controllers within console calls.
    /// </summary>
    public class ControllerAdapter
    {
        public Controller Controller { get; set; }

        public ControllerAdapter(Controller controller)
        {
            Controller = controller;
            Console.WriteLine($"Using adapter on {controller.GetType().Name}.");
        }
    }
}