namespace AdapterPattern
{
    public class XBox : GamingConsole
    {
        public XBox()
        {
            // Associate new XBoxController as default.
            Controller = new XBoxController();
        }
    }
}