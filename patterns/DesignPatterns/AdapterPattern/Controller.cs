namespace AdapterPattern
{
    public interface IController { }

    public class Controller : IController { }

    public class PlayStationController : Controller { }

    public class XBoxController : Controller { }
}