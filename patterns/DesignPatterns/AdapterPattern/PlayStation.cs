namespace AdapterPattern
{
    public class PlayStation : GamingConsole
    {
        public PlayStation()
        {
            Controller = new PlayStationController();
        }
    }
}