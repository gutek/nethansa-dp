using System;

namespace AdapterPattern
{
    interface IGamingConsole
    {
        void Play();
    }

    public class GamingConsole : IGamingConsole
    {
        private Controller _controller;

        /// <summary>
        /// Controller field with custom set method to output controller being activated.
        /// </summary>
        protected Controller Controller
        {
            get => _controller;
            set
            {
                _controller = value;
                Console.WriteLine($"Plugging {Controller.GetType().Name} into {this.GetType().Name} console.");
            }
        }

        public GamingConsole() { }

        public void Play()
        {
            Console.WriteLine($"Playing with {Controller.GetType().Name} on {this.GetType().Name} console.");
        }

        public void Play(ControllerAdapter adapter)
        {
            Controller = adapter.Controller;
            Console.WriteLine($"Playing with {Controller.GetType().Name} on {this.GetType().Name} console.");
        }
    }
}