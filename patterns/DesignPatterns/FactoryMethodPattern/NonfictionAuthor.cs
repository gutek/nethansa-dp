using System;

namespace FactoryMethodPattern
{
    public class NonfictionAuthor : IAuthor
    {
        public void Write()
        {
            Console.WriteLine($"I'm an {this} and I write nonfiction :(");
        }
    }
}