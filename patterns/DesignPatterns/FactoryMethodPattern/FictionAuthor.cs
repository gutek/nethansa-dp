using System;

namespace FactoryMethodPattern
{
    public class FictionAuthor : IAuthor
    {
        public void Write()
        {
            Console.WriteLine($"I'm an {this} and I write fiction :)");
        }
    }
}