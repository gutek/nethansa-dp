namespace FactoryMethodPattern
{
    public class Blog : Publisher
    {
        public override IAuthor CreateAuthor()
        {
            return new FictionAuthor();
        }
    }
}