namespace FactoryMethodPattern
{
    public class Newspaper : Publisher
    {
        public override IAuthor CreateAuthor()
        {
            return new NonfictionAuthor();
        }
    }
}