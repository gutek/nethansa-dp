namespace FactoryMethodPattern
{
    public interface IAuthor
    {
        void Write();
    }
}