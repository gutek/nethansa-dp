using System;

namespace FactoryMethodPattern
{
    class Program
    {
        static void Main(string[] args)
        {
            var blog = new Blog();
            blog.Publish();

            var newspaper = new Newspaper();
            newspaper.Publish();

            Console.ReadKey();
        }
    }
}
