namespace FactoryMethodPattern
{
    public abstract class Publisher
    {
        // maybe some publishers do that ;)
        public abstract IAuthor CreateAuthor();

        public void Publish()
        {
            IAuthor author = CreateAuthor();
            author.Write();
        }
    }
}