using System;

namespace CompositePattern
{
    class Program
    {
        static void Main(string[] args)
        {
            int priceForKill = 1023;
            Console.WriteLine("You have killed the Chrome Monster and gained {0} gold!", priceForKill);

            var tomek = new Person { Name = "Tomek" };
            var pawel = new Person { Name = "Pawel" };
            var ania = new Person { Name = "Ania" };
            var alicja = new Person { Name = "Alicja" };
            var rafal = new Person { Name = "Rafał" };
            var junior = new Person { Name = "Junior" };
            var senior = new Person { Name = "Senior" };

            var bobs = new Group { Members = { junior, senior } };
            var developers = new Group { Name = "Developers", Members = { tomek, pawel, ania, bobs } };

            var parties = new Group { Members = { developers, alicja, rafal } };

            parties.Money += priceForKill;
            parties.Stats();

            Console.ReadKey();
        }
    }
}
