namespace CompositePattern
{
    public interface IParty
    {
        int Money { get; set; }
        void Stats();
    }
}