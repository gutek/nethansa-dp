﻿using System;

namespace CompositePattern
{
    public class Person : IParty
    {
        public string Name { get; set; }
        public int Money { get; set; }
        
        public void Stats()
        {
            Console.WriteLine("{0} have {1:C} gold.", Name, Money);
        }
    }
}
