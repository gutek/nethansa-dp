namespace CommandPattern
{
    public class SubtractCommand : ICommand
    {
        private readonly Calculator _calculator;

        public SubtractCommand(Calculator calculator)
        {
            _calculator = calculator;
        }

        public int Execute()
        {
            return _calculator.Substract();
        }
    }
}