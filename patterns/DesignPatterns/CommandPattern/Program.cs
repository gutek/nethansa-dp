using System;

namespace CommandPattern
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter First Value");
            int a = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Enter Second Value");
            int b = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Enter Commands for Calculator:");
            Console.WriteLine("\t1: for Add");
            Console.WriteLine("\t2: for Substruct");
            string command = Console.ReadLine();

            Invoker invoker = new Invoker();
            Calculator calculator = new Calculator(a, b);

            AddCommand addCommand = new AddCommand(calculator);
            SubtractCommand substractCommand = new SubtractCommand(calculator);

            if (command == "1")
            {
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("Add Command is selected and the Result is {0}", invoker.ExecuteCommand(addCommand));
            }
            else if (command == "2")
            {
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine("Substract Command is selected and the Result is {0}", invoker.ExecuteCommand(substractCommand));
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("You have entered wrong command. Please type Command '1' for addition or '2' for substraction");
            }
            Console.ResetColor();

            Console.ReadKey();
        }
    }
}
