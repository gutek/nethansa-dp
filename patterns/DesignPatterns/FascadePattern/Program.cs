using System;

namespace FascadePattern
{
    class Program
    {
        static void Main(string[] args)
        {
            const string zipCode = "02-788";

            var temperatureFacade = new TemperatureLookupFacade();
            LocalTemperature localTemp = temperatureFacade.GetTemperature(zipCode);

            Console.WriteLine("The current temperature is {1:F1}C/{0:F1}F. in {2}, {3}"
                , localTemp.Farenheit
                , localTemp.Celcius
                , localTemp.City
                , localTemp.State);

            Console.ReadKey();
        }
    }
}
