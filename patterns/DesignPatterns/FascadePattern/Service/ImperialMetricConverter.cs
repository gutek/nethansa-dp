namespace FascadePattern.Service
{
    public class ImperialMetricConverter
    {
        public double CelciousToFarenheit(double degrees)
        {
            return (degrees*9/5) + 32;
        }
    }
}