namespace FascadePattern.Service
{
    public class GeoLookupService
    {
        public Coordinates GetCoordinatesForZipCode(string zipCode)
        {
            return new Coordinates()
                       {
                           Latitude = 52.2297,
                           Longitude = 21.0122
            };
        }

        public class Coordinates
        {
            public double Latitude { get; set; }
            public double Longitude { get; set; }
        }

        public string GetCityForZipCode(string zipCode)
        {
            return "Warsaw";
        }

        public string GetStateForZipCode(string zipCode)
        {
            return "Mazowieckie";
        }

    }
}