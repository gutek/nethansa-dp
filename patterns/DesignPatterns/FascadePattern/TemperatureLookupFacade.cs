using FascadePattern.Service;

namespace FascadePattern
{
    public class TemperatureLookupFacade
    {
        private readonly WeatherService _weatherService;
        private readonly GeoLookupService _geoLookupService;
        private readonly ImperialMetricConverter _converter;

        public TemperatureLookupFacade()
            :this(new WeatherService(), new GeoLookupService(), new ImperialMetricConverter())
        {}

        public TemperatureLookupFacade(WeatherService weatherService
            , GeoLookupService geoLookupService
            , ImperialMetricConverter metricConverter)
        {
            this._weatherService = weatherService;
            this._geoLookupService = geoLookupService;
            this._converter = metricConverter;
        }

        public LocalTemperature GetTemperature(string zipCode)
        {
            var coords = _geoLookupService.GetCoordinatesForZipCode(zipCode);
            var city = _geoLookupService.GetCityForZipCode(zipCode);
            var state = _geoLookupService.GetStateForZipCode(zipCode);
            
            var celsius = _weatherService.GetTempCelsius(coords.Latitude, coords.Longitude);
            var fahrenheit = _converter.CelciousToFarenheit(celsius);

            var localTemperature = new LocalTemperature()
                              {
                                  Farenheit = fahrenheit,
                                  Celcius = celsius,
                                  City = city,
                                  State = state
                              };

            return localTemperature;
        }
    }
}