using System;

namespace SingletonPattern
{
    class Program
    {
        static void Main(string[] args)
        {
            var singleton1 = Singleton.Instance;
            var singleton2 = Singleton.Instance;

            Console.WriteLine($"Reference equal: {ReferenceEquals(singleton2, singleton1)}");

            Console.ReadKey();
        }
    }

    public sealed class Singleton
    {
        private static readonly Singleton _instance = new Singleton("Tomasz");

        static Singleton() { }

        private Singleton(string name) { }

        public string Name { get; }

        public static Singleton Instance => _instance;
    }
}
