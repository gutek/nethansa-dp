namespace FlyweighPattern
{
    public abstract class MailPiece
    {
        public abstract decimal Postage { get; set; }
        public abstract decimal Width { get; set; }
        public abstract decimal Height { get; set; }
        public abstract decimal Thickness { get; set; }

        public int DestinationZip { get; set; }

        public string GetStatistics(int zipCode)
        {
            return $"Zip code is {zipCode}, postage is {Postage} and height is {Height}";
        }
    }

    public class Letter : MailPiece
    {
        public override decimal Postage { get; set; }
        public override decimal Width { get; set; }
        public override decimal Height { get; set; }
        public override decimal Thickness { get; set; }

        public Letter()
        {
            Postage = 0.46M;
            Width = 11.5M;
            Height = 6.125M;
            Thickness = 0.25M;
        }
    }

    public class Postcard : MailPiece
    {
        public override decimal Postage { get; set; }
        public override decimal Width { get; set; }
        public override decimal Height { get; set; }
        public override decimal Thickness { get; set; }

        public Postcard()
        {
            Postage = 0.33M;
            Width = 6.0M;
            Height = 4.25M;
            Thickness = 0.016M;
        }
    }
}