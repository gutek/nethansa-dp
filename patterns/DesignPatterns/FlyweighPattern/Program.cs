using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlyweighPattern
{
    class Program
    {
        static void Main(string[] args)
        {
            // this throws out of memory...
            //var pieces = new List<MailPiece>();
            //for (long index = 0; index < 563000000; index++)
            //{
            //    pieces.Add(new Letter() { DestinationZip = (int)(index % 100000) });
            //}

            var factory = new MailPieceFactory();
            var zipCodes = new List<int>();

            for (int index = 0; index < 20; index++) //563000000
            {
                zipCodes.Add(index % 100000); // how many object we will have
                var randomPiece = factory.GetPiece(GetRandomKey());
                string pieceStatistics = randomPiece.GetStatistics(zipCodes[index]);
                Console.WriteLine(pieceStatistics);
            }

            Console.ReadKey();
        }

        private static char GetRandomKey()
        {
            return new Random().Next() % 2 == 0 ? 'P' : 'L';
        }
    }
}
