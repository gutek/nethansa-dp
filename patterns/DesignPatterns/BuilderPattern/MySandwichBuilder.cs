﻿using System.Collections.Generic;

namespace BuilderPattern
{
    public class MySandwichBuilder : SandwichBuilder
    {
        public override void SetCondiments()
        {
            _sandwich.HasMayo = false;
            _sandwich.HasMustard = true;
        }

        public override void AddVegetables()
        {
            _sandwich.Vegetables = new List<string> { "Tomato" };
        }

        public override void AddCheeseAndMeat()
        {
            _sandwich.CheeseType = CheeseType.Cheddar;
            _sandwich.MeatType = MeatType.Salami;
        }

        public override void PrepareBread()
        {
            _sandwich.BreadType = BreadType.Wheat;
            _sandwich.IsToasted = false;
        }
    }
}
