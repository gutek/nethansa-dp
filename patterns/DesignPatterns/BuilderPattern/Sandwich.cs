﻿using System;
using System.Collections.Generic;

namespace BuilderPattern
{
    public class Sandwich
    {
        public BreadType BreadType { get; set; }
        public bool IsToasted { get; set; }
        public CheeseType CheeseType { get; set; }
        public MeatType MeatType { get; set; }
        public bool HasMustard { get; set; }
        public bool HasMayo { get; set; }
        public List<string> Vegetables { get; set; }

        public void Display()
        {
            Console.WriteLine("Sandwich on {0} bread", BreadType);
            if(IsToasted)
                Console.WriteLine("\tToasted");
            if (HasMayo)
                Console.WriteLine("\tWith Mayo");
            if (HasMustard)
                Console.WriteLine("\tWith Mustard");
            Console.WriteLine("\tMeat: {0}", MeatType);
            Console.WriteLine("\tCheese: {0}", CheeseType);
            Console.WriteLine("\tVeggies:");
            foreach(var vegetable in Vegetables)
                Console.WriteLine("\t   {0}", vegetable);
        }
    }
}
