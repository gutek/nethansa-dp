﻿namespace BuilderPattern
{
    public enum CheeseType
    {
        Swiss,
        Cheddar
    }
}