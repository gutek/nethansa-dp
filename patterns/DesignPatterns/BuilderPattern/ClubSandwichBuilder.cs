using System.Collections.Generic;

namespace BuilderPattern
{
    public class ClubSandwichBuilder : SandwichBuilder
    {
        public override void SetCondiments()
        {
            _sandwich.HasMayo = true;
            _sandwich.HasMustard = true;
        }

        public override void AddVegetables()
        {
            _sandwich.Vegetables = new List<string> { "Tomato", "Onion", "Lettuce" };
        }

        public override void AddCheeseAndMeat()
        {
            _sandwich.CheeseType = CheeseType.Swiss;
            _sandwich.MeatType = MeatType.Ham;
        }

        public override void PrepareBread()
        {
            _sandwich.BreadType = BreadType.White;
            _sandwich.IsToasted = true;
        }
    }
}