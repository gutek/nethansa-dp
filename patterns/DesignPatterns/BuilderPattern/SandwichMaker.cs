﻿namespace BuilderPattern
{
    public class SandwichMaker
    {
        private readonly SandwichBuilder _builder;

        public SandwichMaker(SandwichBuilder builder)
        {
            this._builder = builder;
        }

        public void BuildSandwich()
        {
            _builder.CreateNewSandwich();
            _builder.PrepareBread();
            _builder.AddCheeseAndMeat();
            _builder.AddVegetables();
            _builder.SetCondiments();
        }

        public Sandwich GetSandwhich()
        {
            return _builder.Build();
        }
    }
}
