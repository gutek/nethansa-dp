﻿using System;

namespace BuilderPattern
{
    class SuperSandwichBuilder : SandwichBuilder
    {
        public override void PrepareBread()
        {
            throw new NotImplementedException();
        }

        public override void AddCheeseAndMeat()
        {
            throw new NotImplementedException();
        }

        public override void AddVegetables()
        {
            throw new NotImplementedException();
        }

        public override void SetCondiments()
        {
            throw new NotImplementedException();
        }
    }
}
