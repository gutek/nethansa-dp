﻿namespace BuilderPattern
{
    public abstract class SandwichBuilder
    {
        protected Sandwich _sandwich;

        // or get or make
        public Sandwich Build()
        {
            return _sandwich;
        }

        public void CreateNewSandwich()
        {
             _sandwich = new Sandwich();
        }

        public abstract void PrepareBread();
        public abstract void AddCheeseAndMeat();
        public abstract void AddVegetables();
        public abstract void SetCondiments();
    }
}
