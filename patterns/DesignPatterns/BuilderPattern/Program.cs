using System;

namespace BuilderPattern
{
    class Program
    {
        static void Main(string[] args)
        {
            SandwichBuilder builder = new MySandwichBuilder();

            var sandwichMaker = new SandwichMaker(builder);
            sandwichMaker.BuildSandwich();
            var sandwich1 = sandwichMaker.GetSandwhich();

            sandwich1.Display();

            Console.WriteLine("------------------------");

            builder = new ClubSandwichBuilder();
            var sandwichMaker2 = new SandwichMaker(builder);
            sandwichMaker2.BuildSandwich();
            var sandwich2 = sandwichMaker2.GetSandwhich();

            sandwich2.Display();

            Console.ReadKey();
        }
    }
}
