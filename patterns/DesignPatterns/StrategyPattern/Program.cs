using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StrategyPattern
{
    class Program
    {
        static void Main(string[] args)
        {
            Func<Order, decimal> inpostStrategy = CalcForInpost;
            Func<Order, decimal> upsStrategy = order => 3.25m;

            /// local function
            decimal DhlStrategy(Order order) => 4.25m;

            var localOrder = Constructor.CreateOrder();

            var calculatorService = new ShippingCostCalculatorService();
            Console.WriteLine($"Inpost Shipping Cost: {calculatorService.CalculateShippingCost(localOrder, inpostStrategy)}");
            Console.WriteLine($"UPS Shipping Cost: {calculatorService.CalculateShippingCost(localOrder, upsStrategy)}");
            Console.WriteLine($"DHL Shipping Cost: {calculatorService.CalculateShippingCost(localOrder, DhlStrategy)}");


            Console.ReadKey();
        }


        internal static decimal CalcForInpost(Order arg)
        {
            return 5.00m;
        }
    }
}
