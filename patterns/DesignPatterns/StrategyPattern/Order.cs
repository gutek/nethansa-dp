﻿using StrategyPattern;

namespace StrategyPattern
{
    public class Order
    {
        public Address Destination { get; set; }
        public Address Origin { get; set; }
    }
}