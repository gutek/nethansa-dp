namespace StrategyPattern
{
    public static class Constructor
    {

        public static Order CreateOrder()
        {
            return new Order()
            {
                Destination = Constructor.CreateAddress_Destination(),
                Origin = Constructor.CreateAddress_Origin()
            };
        }

        public static Address CreateAddress_Origin()
        {
            return new Address()
            {
                ContactName = "Tomasz Kot",

                AddressLine1 = "Pulawska 128",
                AddressLine2 = null,
                AddressLine3 = null,
                City = "Warszawa",
                Country = "Polska",
                Region = "MAZ",
                PostalCode = "02-999"
            };
        }

        public static Address CreateAddress_Destination()
        {
            return new Address()
            {
                ContactName = "Agnieszka Maciag",

                AddressLine1 = "Zurawia 129",
                AddressLine2 = null,
                AddressLine3 = null,
                City = "Warszawa",
                Country = "Polska",
                Region = "MAZ",
                PostalCode = "05-555"
            };
        }
    }
}