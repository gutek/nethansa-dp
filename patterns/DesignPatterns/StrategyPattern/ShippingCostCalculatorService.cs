﻿

using System;

namespace StrategyPattern
{
    // instead of Func, we could use concrate class
    /*
    public class ShippingCostCalculatorService
    {
        readonly IShippingCostStrategy _strategy;

        public ShippingCostCalculatorService(IShippingCostStrategy strategy)
        {
            _strategy = strategy;
        }

        public decimal CalculateShippingCost(Order order)
        {
           return _strategy.Calculate(order);
        }
    }

    public interface IShippingCostStrategy
    {
        decimal Calculate(Order order);
    }
     */

    public class ShippingCostCalculatorService
    {
        public decimal CalculateShippingCost(Order order, Func<Order, decimal> strategy)
        {
           return strategy(order);
        }
    }
}
