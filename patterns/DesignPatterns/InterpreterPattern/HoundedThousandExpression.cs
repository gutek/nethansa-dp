namespace InterpreterPattern
{
    public class HoundedThousandExpression : Expression
    {
        public override string One() { return "C"; }
        public override string Four() { return " "; }
        public override string Five() { return "D"; }
        public override string Nine() { return " "; }
        public override int Multiplier() { return 100000; }
    }
}