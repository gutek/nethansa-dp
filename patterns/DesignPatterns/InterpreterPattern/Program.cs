using System;
using System.Collections.Generic;

namespace InterpreterPattern
{
    class Program
    {
        static void Main(string[] args)
        {
            var roman = "MMXIX";
            var context = new Context(roman);

            // Build the 'parse tree'
            var tree = new List<Expression>
            {
                //new MillionExpression()
                //, new HoundedThousandExpression(),
                 new ThousandExpression()
                , new ThousandExpression()
                , new HundredExpression()
                , new TenExpression()
                , new OneExpression()
            };

            // Interpret
            foreach (var exp in tree)
            {
                exp.Interpret(context);
            }

            Console.WriteLine("{0} = {1}", roman, context.Output);

            // Wait for user
            Console.ReadKey();
        }
    }
}
