# dotnet tdd

Materiały szkoleniowe dot. szkolenia Wzorce Projektowe przeprowadzonego w dniach 2019-04-16/17.

## STRUKTURA

* **patterns** - zbiór przykładów wzorców projektowych.
* **slides** - slajdy z warsztatów w wersji offline, link do wersji online ponzej.

## ANKIETA

* [ankieta](http://bit.ly/nethansa-ankieta)

## LINKI

### PREZENTACJE

* [design patterns](https://slides.com/gutek/nethansa-design-patterns?token=458TipPH)
* [Kubernetes dla Programistów (prezentacja)](https://slides.com/gutek/k8s-for-dev?token=05KriCsW)